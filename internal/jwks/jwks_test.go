// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package jwks

import (
	"crypto/rsa"
	"encoding/json"
	"errors"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/gljobctx-go/internal/web"
	"gitlab.com/ecp-ci/gljobctx-go/test/mocks/mock_web"
)

func TestCheckHeader(t *testing.T) {
	tests := map[string]struct {
		encoded       string
		assertResults func(*testing.T, Header, error)
	}{
		"empty string": {
			encoded: "",
			assertResults: func(t *testing.T, header Header, err error) {
				assert.Error(t, err)
			},
		},
		"invalid base64": {
			encoded: "*ABC123",
			assertResults: func(t *testing.T, header Header, err error) {
				assert.EqualError(t, err, "illegal base64 data at input byte 0")
			},
		},
		"invalid json": {
			encoded: "SGVsbG8sIFdvcmxkCg==",
			assertResults: func(t *testing.T, header Header, err error) {
				assert.Error(t, err)
			},
		},
		"invalid alg": {
			encoded: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ",
			assertResults: func(t *testing.T, header Header, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "validation for 'Alg' failed")
				}
			},
		},
		"correct header": {
			encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ",
			assertResults: func(t *testing.T, header Header, err error) {
				assert.NoError(t, err)
				assert.Equal(t, Header{
					Alg: "RS256",
					Kid: "test",
					Typ: "JWT",
				}, header)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := CheckHeader(tt.encoded)

			if tt.assertResults != nil {
				tt.assertResults(t, got, err)
			}
		})
	}
}

func TestFetchKey(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		client        func(ctrl *gomock.Controller) web.Client
		url           string
		kid           string
		alg           string
		assertResults func(*testing.T, interface{}, error)
	}{
		"bad request": {
			client: func(ctrl *gomock.Controller) web.Client {
				m := mock_web.NewMockClient(ctrl)
				m.EXPECT().GetJSON("test.bad/-/jwks", gomock.Any(), &WebKeySet{}).
					Return(errors.New("bad request message"))
				return m
			},
			url: "test.bad/-/jwks",
			assertResults: func(t *testing.T, i interface{}, err error) {
				assert.EqualError(t, err, "bad request message")
			},
		},
		"missing kid": {
			client: func(ctrl *gomock.Controller) web.Client {
				m := mock_web.NewMockClient(ctrl)
				m.EXPECT().GetJSON("test.missing/-/jwks", gomock.Any(), &WebKeySet{}).DoAndReturn(
					func(tarURL string, x interface{}, v interface{}) interface{} {
						_ = json.NewDecoder(
							strings.NewReader(`{"keys":[{"kty":"RSA","kid":"missing","e":"AQAB","n":"nzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA-kzeVOVpVWwkWdVha4s38XM_pa_yr47av7-z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr_Mrm_YtjCZVWgaOYIhwrXwKLqPr_11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e-lf4s4OxQawWD79J9_5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa-GSYOD2QU68Mb59oSk2OB-BtOLpJofmbGEGgvmwyCI9Mw", "use":"sig","alg":"RS256"}]}`),
						).Decode(v)
						return nil
					},
				)
				return m
			},
			url: "test.missing/-/jwks",
			kid: "test",
			alg: "RS256",
			assertResults: func(t *testing.T, i interface{}, err error) {
				assert.EqualError(t, err, "no matching entry for kid test found")
			},
		},
		"no exponent": {
			client: func(ctrl *gomock.Controller) web.Client {
				m := mock_web.NewMockClient(ctrl)
				m.EXPECT().GetJSON("test.no-exponent/-/jwks", gomock.Any(), &WebKeySet{}).DoAndReturn(
					func(tarURL string, x interface{}, v interface{}) interface{} {
						_ = json.NewDecoder(
							strings.NewReader(`{"keys":[{"kty":"RSA","kid":"test","n":"nzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA-kzeVOVpVWwkWdVha4s38XM_pa_yr47av7-z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr_Mrm_YtjCZVWgaOYIhwrXwKLqPr_11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e-lf4s4OxQawWD79J9_5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa-GSYOD2QU68Mb59oSk2OB-BtOLpJofmbGEGgvmwyCI9Mw", "use":"sig","alg":"RS256"}]}`),
						).Decode(v)
						return nil
					},
				)
				return m
			},
			url: "test.no-exponent/-/jwks",
			kid: "test",
			alg: "RS256",
			assertResults: func(t *testing.T, i interface{}, err error) {
				assert.EqualError(t, err, "failed to identify public key, verify JWKS algorithm type")
			},
		},
		"no modulus": {
			client: func(ctrl *gomock.Controller) web.Client {
				m := mock_web.NewMockClient(ctrl)
				m.EXPECT().GetJSON("test.no-modulus/-/jwks", gomock.Any(), &WebKeySet{}).DoAndReturn(
					func(tarURL string, x interface{}, v interface{}) interface{} {
						_ = json.NewDecoder(
							strings.NewReader(`{"keys":[{"kty":"RSA","kid":"test","e":"AQAB","use":"sig","alg":"RS256"}]}`),
						).Decode(v)
						return nil
					},
				)
				return m
			},
			url: "test.no-modulus/-/jwks",
			kid: "test",
			alg: "RS256",
			assertResults: func(t *testing.T, i interface{}, err error) {
				assert.EqualError(t, err, "failed to identify public key, verify JWKS algorithm type")
			},
		},
		"alg does not match": {
			client: func(ctrl *gomock.Controller) web.Client {
				m := mock_web.NewMockClient(ctrl)
				m.EXPECT().GetJSON("test.pass/-/jwks", gomock.Any(), &WebKeySet{}).DoAndReturn(
					func(tarURL string, x interface{}, v interface{}) interface{} {
						_ = json.NewDecoder(
							strings.NewReader(`{"keys":[{"kty":"RSA","kid":"test","e":"AQAB","n":"u1SU1LfVLPHCozMxH2Mo4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0_IzW7yWR7QkrmBL7jTKEn5u-qKhbwKfBstIs-bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyehkd3qqGElvW_VDL5AaWTg0nLVkjRo9z-40RQzuVaE8AkAFmxZzow3x-VJYKdjykkJ0iT9wCS0DRTXu269V264Vf_3jvredZiKRkgwlL9xNAwxXFg0x_XFw005UWVRIkdgcKWTjpBP2dPwVZ4WWC-9aGVd-Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbcmw", "use":"sig","alg":"RS256"}]}`),
						).Decode(v)
						return nil
					},
				).AnyTimes()
				return m
			},
			url: "test.pass/-/jwks",
			kid: "test",
			alg: "RS512",
			assertResults: func(t *testing.T, i interface{}, err error) {
				assert.EqualError(t, err, "algorithm expected by JWKS (RS256) does not match JWT (RS512)")
			},
		},
		"correct key identified": {
			client: func(ctrl *gomock.Controller) web.Client {
				m := mock_web.NewMockClient(ctrl)
				m.EXPECT().GetJSON("test.pass/-/jwks", gomock.Any(), &WebKeySet{}).DoAndReturn(
					func(tarURL string, x interface{}, v interface{}) interface{} {
						_ = json.NewDecoder(
							strings.NewReader(`{"keys":[{"kty":"RSA","kid":"test","e":"AQAB","n":"u1SU1LfVLPHCozMxH2Mo4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0_IzW7yWR7QkrmBL7jTKEn5u-qKhbwKfBstIs-bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyehkd3qqGElvW_VDL5AaWTg0nLVkjRo9z-40RQzuVaE8AkAFmxZzow3x-VJYKdjykkJ0iT9wCS0DRTXu269V264Vf_3jvredZiKRkgwlL9xNAwxXFg0x_XFw005UWVRIkdgcKWTjpBP2dPwVZ4WWC-9aGVd-Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbcmw", "use":"sig","alg":"RS256"}]}`),
						).Decode(v)
						return nil
					},
				).AnyTimes()
				return m
			},
			url: "test.pass/-/jwks",
			kid: "test",
			alg: "RS256",
			assertResults: func(t *testing.T, i interface{}, err error) {
				assert.NoError(t, err)
				assert.NotNil(t, i)
				assert.IsType(t, &rsa.PublicKey{}, i)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := FetchKey(tt.client(ctrl), tt.url, tt.kid, tt.alg)

			if tt.assertResults != nil {
				tt.assertResults(t, got, err)
			}
		})
	}
}

// Verify errors are observed when unmarshalling any JSON.
func Test_UnmarshalJSON(t *testing.T) {
	t.Run("module invalid JSON", func(t *testing.T) {
		m := &modulus{}
		err := m.UnmarshalJSON([]byte{})
		assert.EqualError(t, err, "unexpected end of JSON input")
	})

	t.Run("modulus invalid base64", func(t *testing.T) {
		m := &modulus{}
		err := m.UnmarshalJSON([]byte("\"%A1\""))
		assert.EqualError(t, err, "illegal base64 data at input byte 0")
	})

	t.Run("exponent invalid JSON", func(t *testing.T) {
		e := &exponent{}
		err := e.UnmarshalJSON([]byte{})
		assert.EqualError(t, err, "unexpected end of JSON input")
	})

	t.Run("exponent invalid base64", func(t *testing.T) {
		e := &exponent{}
		err := e.UnmarshalJSON([]byte("\"%A1\""))
		assert.EqualError(t, err, "illegal base64 data at input byte 0")
	})
}
