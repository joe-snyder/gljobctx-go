// SPDX-License-Identifier: (Apache-2.0 OR MIT)

// Package rules maintains established validation for shared aspects relating
// to either Jacamar specifically or the ECP CI effort as a whole. All checks
// that have been defined realize the validator.Func interface for the
// github.com/go-playground/validator/v10 and are meant to be a component in
// any validation strategy.
//
// Example code:
//
//      type Header struct {
//      	Alg string `json:"alg" validate:"len=5,alphanum"`
//	        Kid string `json:"kid" validate:"kid"`
//	        Typ string `json:"typ" validate:"eq=JWT"`
//      }
//
//      func Assert(h Header) error {
//          v := validator.New()
//	        _ = v.RegisterValidation("kid", rules.CheckKID)
//          return v.Struct(h)
//      }
//
// When using any rules within this package ensure that the desired value is
// optional or required. Many rules established are labeled as optional, meaning
// the lack of an expected value is acceptable and the maximum length should always
// be enforced via the go-playground/validator maximum structure flag. In these cases
// combining rules others found in validator package may be required. Additionally,
// always check the expected value type outlined in the function documentation. Type
// conversations are not handled and strict expectation are required to avoid failures.
//
package rules

import (
	"reflect"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
)

// CheckUsername optionally examines the provided username for validity based upon
// the GitLab server requirements while observing potentially egregious unix
// characters. Implements validator.Func for String types only.
func CheckUsername(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	matched, _ := regexp.MatchString(
		`^([a-zA-Z0-9][a-zA-Z0-9._-]{0,254})$`,
		v.Field().String(),
	)
	return matched
}

// CheckProjectPath ensures the values adheres to expectations of a GitLab group/project.,
// implements validator.Func. Path can contain only letters, digits, '_', '-' and '.'.
// Cannot start with '-', end in '.git' or end in '.atom'.
func CheckProjectPath(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	str := v.Field().String()
	if strings.HasPrefix(str, "-") ||
		strings.HasSuffix(str, ".git") ||
		strings.HasSuffix(str, ".atom") {
		return false
	}

	matched, _ := regexp.MatchString(`^[a-zA-Z0-9][a-zA-Z0-9-_./+]+$`, str)

	return matched
}

// CheckKID ensures a valid Key ID in a JWT header, implement validator.Func for string type.
func CheckKID(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	matched, _ := regexp.MatchString(`^[a-zA-Z0-9\-_]+$`, v.Field().String())

	return matched
}

// CheckPipelineSource verifies the structure of a CI_PIPELINE_SOURCE string and no potentially
// illegal characters. The string itself is not checked ot ensure it is recognized as a source.
// Implements validator.Func for String types only.
func CheckPipelineSource(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	matched, _ := regexp.MatchString(
		`^[a-zA-Z_\-]*$`,
		v.Field().String(),
	)

	return matched
}

// CheckRef enforces very limited checks regarding an incoming ref/type/environment
// for basic validity. This does not ensure that it is functional or accepted across
// all potential sources. Implements validator.Func for String types only.
func CheckRef(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	str := v.Field().String()

	// Golang does not support negative look around?
	matched := strings.Contains(str, `\`) ||
		strings.Contains(str, `..`) ||
		strings.Contains(str, `$`) ||
		strings.Contains(str, `|`)

	return !matched
}

// CheckURL validates a potential url with valid characters only for
// the JWTs 'aud' or 'iss'. In older versions of the JWT 'aud' is not
// required and the 'iss' may not align with the requirements of a valid
// URL parse in Go. Implements validator.Func for String types only.
func CheckURL(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	str := v.Field().String()
	if str == "" {
		return true
	}

	// https://www.ietf.org/rfc/rfc3986.txt
	// We should not accept any delimiting characters that may be used
	// outside the host or immediate path of a valid URL. In some older
	// cases a http prefix might not be included.
	matched, _ := regexp.MatchString(
		`^[a-zA-Z0-9.\-+_:/]+$`,
		v.Field().String(),
	)

	return matched
}
