// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package rules_test

import (
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/gljobctx-go/internal/rules"
)

func TestCheckUserName(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("username", rules.CheckUsername)

	type myTest struct {
		Name string `validate:"username"`
	}

	tests := map[string]struct {
		username string
		wantErr  bool
	}{
		"empty": {
			username: "",
			wantErr:  true,
		},
		"greater than 255 character": {
			username: "abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123",
			wantErr:  true,
		},
		"hyphen in middle": {
			username: "user-name01",
			wantErr:  false,
		},
		"alphanumeric": {
			username: "user123",
			wantErr:  false,
		},
		"underscore in middle": {
			username: "user_name01",
			wantErr:  false,
		},
		"space in middle": {
			username: "user name01",
			wantErr:  true,
		},
		"bash command": {
			username: "user$(env)",
			wantErr:  true,
		},
		"period starting": {
			username: ".username",
			wantErr:  true,
		},
		"period in middle": {
			username: "user.name",
			wantErr:  false,
		},
		"underscore starting": {
			username: "_username",
			wantErr:  true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Name: tt.username,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckUsername() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"username"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestProjectPath(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("projectPath", rules.CheckProjectPath)

	type myTest struct {
		Path string `validate:"projectPath"`
	}

	tests := map[string]struct {
		path    string
		wantErr bool
	}{
		"empty": {
			path:    "",
			wantErr: true,
		},
		"end with .git": {
			path:    "group/project.git",
			wantErr: true,
		},
		"end with .atom": {
			path:    "group/project.atom",
			wantErr: true,
		},
		"begin with -": {
			path:    "-group/project",
			wantErr: true,
		},
		"invalid characters": {
			path:    "group/$(project)/1",
			wantErr: true,
		},
		"valid project path": {
			path:    "group/sub-group.1/my_project+test-1",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Path: tt.path,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckProjectPath() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"projectPath"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestKID(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("kid", rules.CheckKID)

	type myTest struct {
		ID string `validate:"kid"`
	}

	tests := map[string]struct {
		id      string
		wantErr bool
	}{
		"empty": {
			id:      "",
			wantErr: true,
		},
		"invalid characters": {
			id:      "group/$(project)/1",
			wantErr: true,
		},
		"valid kid (gitlab.com/-/jwks - 0)": {
			id:      "kewiQq9jiC84CvSsJYOB-N6A8WFLSV20Mb-y7IlWDSQ",
			wantErr: false,
		},
		"valid kid (gitlab.com/-/jwks - 1)": {
			id:      "4i3sFE7sxqNPOT7FdvcGA1ZVGGI_r-tsDXnEuYT4ZqE",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				ID: tt.id,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckKID() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"kid"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestPipelineSource(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("pipelineSource", rules.CheckPipelineSource)

	type myTest struct {
		Source string `validate:"pipelineSource"`
	}

	tests := map[string]struct {
		source  string
		wantErr bool
	}{
		"empty": {
			source:  "",
			wantErr: false,
		},
		"invalid, alphanumeric": {
			source:  "EArXoCj8gmiAa8eZzPiR",
			wantErr: true,
		},
		"invalid, character encountered": {
			source:  "PG_buQjszuoCoxxayMZG$\\",
			wantErr: true,
		},
		"valid, lowercase and underscore encountered": {
			source:  "pg_buqjszuocoxxaymzg",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Source: tt.source,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckPipelineSource() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"pipelineSource"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestRef(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("ref", rules.CheckRef)

	type myTest struct {
		Ref string `validate:"ref"`
	}

	tests := map[string]struct {
		ref     string
		wantErr bool
	}{
		"empty": {
			ref:     "",
			wantErr: false,
		},
		"multiple periods": {
			ref:     "git...commit",
			wantErr: true,
		},
		"backslashes": {
			ref:     "//\\\\\\test",
			wantErr: true,
		},
		"valid ref": {
			ref:     "Git_commit-example-123",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Ref: tt.ref,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckRef() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Ref int `validate:"ref"`
		}
		err := validate.Struct(typeTest{
			Ref: 1,
		})
		assert.Error(t, err)
	})
}

func TestURL(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("url", rules.CheckURL)

	type myTest struct {
		URL string `validate:"url"`
	}

	tests := map[string]struct {
		urlStr  string
		wantErr bool
	}{
		"empty": {
			urlStr:  "",
			wantErr: false,
		},
		"valid url": {
			urlStr:  "https://example.com",
			wantErr: false,
		},
		"valid IP address": {
			urlStr:  "http://127.0.0.1:5000",
			wantErr: false,
		},
		"invalid url in iss": {
			urlStr:  "localhost",
			wantErr: false,
		},
		"subdomain V1 iss": {
			urlStr:  "example.com/test",
			wantErr: false,
		},
		"IP address only": {
			urlStr:  "127.0.0.1",
			wantErr: false,
		},
		"invalid bash characters": {
			urlStr:  "https://example$(test).com",
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				URL: tt.urlStr,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckURL() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			URL int `validate:"url"`
		}
		err := validate.Struct(typeTest{
			URL: 1,
		})
		assert.Error(t, err)
	})
}
