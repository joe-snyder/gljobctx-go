## v0.2.1 (April 22, 2022)

* Improve rules validation for `env`
  ([!13](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/13))

## v0.2.0 (April 18, 2022)

* Clarify `Claims` validation option and supported payload
  ([!11](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/11))
* Add `Validator` interface and correct mocks
  ([!10](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/10),
  [!9](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/9))
* Update support/testing to Go 1.18.1
  ([!8](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/8))

## v0.1.0 (April 7, 2022)

* Initial release
