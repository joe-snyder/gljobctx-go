# Job Context Validation

[![pipeline status](https://gitlab.com/ecp-ci/gljobctx-go/badges/main/pipeline.svg)](https://gitlab.com/ecp-ci/gljobctx-go/-/commits/main)
[![coverage report](https://gitlab.com/ecp-ci/gljobctx-go/badges/main/coverage.svg)](https://gitlab.com/ecp-ci/gljobctx-go/-/commits/main)

A GoLang package for validating the `CI_JOB_JWT` provided by the
[GitLab server during CI/CD](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

*NOTE*: This repository is still under development and is subject to
modifications. Not all potential claims are currently supported.
