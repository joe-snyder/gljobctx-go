// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package gljobctx

import (
	"errors"
	"fmt"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt/v4"

	"gitlab.com/ecp-ci/gljobctx-go/internal/rules"
)

var v *validator.Validate

// Claims details CI job level information established based upon a verified CI_JOB_JWT.
// Source: https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
type Claims struct {
	NamespaceID    string `json:"namespace_id" validate:"required,max=10,number" env:"JWT_NAMESPACE_ID"`
	NamespacePath  string `json:"namespace_path" validate:"required,max=500,projectPath" env:"JWT_NAMESPACE_PATH"`
	ProjectID      string `json:"project_id" validate:"required,max=10,number" env:"JWT_PROJECT_ID"`
	ProjectPath    string `json:"project_path" validate:"max=500,projectPath" env:"JWT_PROJECT_PATH"`
	UserID         string `json:"user_id" validate:"required,max=10,number" env:"JWT_USER_ID"`
	UserLogin      string `json:"user_login" validate:"required,max=250,username" env:"JWT_USER_LOGIN"`
	UserEmail      string `json:"user_email" validate:"max=250,email" env:"JWT_USER_EMAIL"`
	PipelineID     string `json:"pipeline_id" validate:"required,max=10,number" env:"JWT_PIPELINE_ID"`
	PipelineSource string `json:"pipeline_source" validate:"max=100,pipelineSource" env:"JWT_PIPELINE_SOURCE"`
	JobID          string `json:"job_id" validate:"required,max=10,number" env:"JWT_JOB_ID"`
	Ref            string `json:"ref" validate:"required,max=250,ref" env:"JWT_REF"`
	RefType        string `json:"ref_type" validate:"required,max=50,ref" env:"JWT_REF_TYPE"`
	RefProtected   string `json:"ref_protected" validate:"required,boolean" env:"JWT_REF_PROTECTED"`

	// OverrideClaims https://tools.ietf.org/html/rfc7519#section-4.1
	OverrideClaims
}

type OverrideClaims struct {
	// Issuer (iss) https://tools.ietf.org/html/rfc7519#section-4.1
	Issuer string `json:"iss,omitempty" validate:"max=125,potentialURL"`
	// Audience (aud) https://tools.ietf.org/html/rfc7519#section-4.1
	Audience string `json:"aud,omitempty" validate:"max=125,potentialURL"`
	// Subject (sub) https://tools.ietf.org/html/rfc7519#section-4.1
	Subject string `json:"sub,max=500,omitempty" validate:"required,ref"`
	// ExpiresAt (exp) https://tools.ietf.org/html/rfc7519#section-4.1
	ExpiresAt *jwt.NumericDate `json:"exp,omitempty"`
	// NotBefore (nbf) https://tools.ietf.org/html/rfc7519#section-4.1
	NotBefore *jwt.NumericDate `json:"nbf,omitempty"`
	// IssuedAt (iat) https://tools.ietf.org/html/rfc7519#section-4.1
	IssuedAt *jwt.NumericDate `json:"iat,omitempty"`
}

func (c Claims) verifyExpiresAt(now time.Time, delay time.Duration) error {
	if c.ExpiresAt == nil {
		return errors.New("missing expiration (exp)")
	}

	if delay > 0*time.Second {
		c.ExpiresAt.Time = c.ExpiresAt.Time.Add(delay)
	}

	if !now.Before(c.ExpiresAt.Time) {
		diff := now.Sub(c.ExpiresAt.Time)
		round, _ := time.ParseDuration("1m")
		return fmt.Errorf("token expired by: %v", diff.Round(round).String())
	}

	return nil
}

func (c Claims) verifyNotBefore(now time.Time) error {
	if c.NotBefore == nil {
		return errors.New("missing not valid before (nbf)")
	}

	// Provide an additional 30-second window to for minor differences.
	now = now.Add(30 * time.Second)
	if now.Before(c.NotBefore.Time) {
		diff := c.NotBefore.Time.Sub(now)
		round, _ := time.ParseDuration("1s")
		return fmt.Errorf("token used before issued by: %v", diff.Round(round).String())
	}

	return nil
}

func (c Claims) verifySubject(v1jwt bool) (err error) {
	if c.Subject == "" {
		err = errors.New("missing subject (sub)")
	} else if v1jwt && c.Subject != fmt.Sprintf("job_%s", c.JobID) {
		err = errors.New("subject (sub) conflicts with JobID")
	}

	// With JWT V2 we do not directly validate the sub as by this point the JWT
	// is already trusted in the context of the known job. Environment validation
	// simply treats this value as any ref/path.

	return
}

func (c Claims) verifyJobID(trustedID string) (err error) {
	if c.JobID == "" {
		err = errors.New("missing Job ID (job_id)")
	} else if c.JobID != trustedID {
		err = errors.New("stolen JWT detected")
	}

	return
}

func (j job) parseClaims(key interface{}) (jc Claims, err error) {
	token, err := jwt.ParseWithClaims(j.encoded, &jc, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			// We've previously validated the 'alg' in the Header structure.
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return key, nil
	})

	if err == nil && token.Valid {
		err = j.verifications(jc)
	}

	return
}

// Valid realizes the jwt.Claims interface.
func (o OverrideClaims) Valid() error {
	// Pass any registered claims that will be left unverified to the JWT package. Even with modification
	rc := jwt.RegisteredClaims{
		Subject:  o.Subject,
		IssuedAt: o.IssuedAt,
	}

	return rc.Valid()
}

// verifications enforces all CI_JOB_JWT specific verification steps, not handled
// by the 'golang-jwt/jwt' package.
func (j job) verifications(jc Claims) (err error) {
	now := time.Now()

	if err = jc.verifyExpiresAt(now, j.expDelay); err != nil {
		return
	} else if err = jc.verifyNotBefore(now); err != nil {
		return
	} else if err = jc.verifyJobID(j.jobID); err != nil {
		return
	} else if err = jc.verifySubject(j.supportJWTV1); err != nil {
		return
	}

	if j.claimEnvValidation {
		err = v.Struct(jc)
	}

	return
}

func init() {
	v = validator.New()

	_ = v.RegisterValidation("username", rules.CheckUsername)
	_ = v.RegisterValidation("projectPath", rules.CheckProjectPath)
	_ = v.RegisterValidation("pipelineSource", rules.CheckPipelineSource)
	_ = v.RegisterValidation("ref", rules.CheckRef)
	_ = v.RegisterValidation("potentialURL", rules.CheckURL)
}
