// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package gljobctx

import (
	"crypto/rsa"
	"math/big"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"github.com/stretchr/testify/assert"
)

func TestClaims_verifyExpiresAt(t *testing.T) {
	now := time.Now()

	tests := map[string]jwtTest{
		"missing expiration": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "missing expiration (exp)")
			},
		},
		"no delay defined": {
			claims: Claims{
				OverrideClaims: OverrideClaims{
					ExpiresAt: &jwt.NumericDate{
						Time: now.Add(1 * time.Hour),
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"delayed defined and encountered": {
			claims: Claims{
				OverrideClaims: OverrideClaims{
					ExpiresAt: &jwt.NumericDate{
						Time: now.Add(10 * time.Minute),
					},
				},
			},
			expDelay: 20 * time.Minute,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"expired token": {
			claims: Claims{
				OverrideClaims: OverrideClaims{
					ExpiresAt: &jwt.NumericDate{
						Time: time.Unix(100000, 0),
					},
				},
			},
			expDelay: 20 * time.Minute,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.claims.verifyExpiresAt(now, tt.expDelay)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestClaims_verifyNotBefore(t *testing.T) {
	now := time.Now()

	tests := map[string]jwtTest{
		"nbf not defined": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "missing not valid before (nbf)")
			},
		},
		"nbf outside 30 second window": {
			claims: Claims{
				OverrideClaims: OverrideClaims{
					NotBefore: &jwt.NumericDate{
						Time: now.Add(1 * time.Hour),
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "token used before issued by")
				}
			},
		},
		"nbf inside 30 second window": {
			claims: Claims{
				OverrideClaims: OverrideClaims{
					NotBefore: &jwt.NumericDate{
						Time: now.Add(10 * time.Second),
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.claims.verifyNotBefore(now)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestClaims_verifySubject(t *testing.T) {
	tests := map[string]jwtTest{
		"no subject in claims": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "missing subject (sub)")
			},
		},
		"subject conflicts with id": {
			claims: Claims{
				JobID: "111",
				OverrideClaims: OverrideClaims{
					Subject: "job_{id}",
				},
			},
			job: job{
				supportJWTV1: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "subject (sub) conflicts with JobID")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.claims.verifySubject(tt.job.supportJWTV1)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestClaims_verifyJobID(t *testing.T) {
	tests := map[string]jwtTest{
		"no id in claims": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "missing Job ID (job_id)")
			},
		},
		"stolen jwt": {
			jobID: "123",
			claims: Claims{
				JobID: "456",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "stolen JWT detected")
			},
		},
		"valid id": {
			jobID: "10001",
			claims: Claims{
				JobID: "10001",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.claims.verifyJobID(tt.jobID)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestOverrideClaims_Valid(t *testing.T) {
	t.Run("failures from jwt package observed", func(t *testing.T) {
		err := OverrideClaims{
			IssuedAt: &jwt.NumericDate{
				time.Now().Add(1 * time.Hour),
			},
		}.Valid()
		assert.Error(t, err)
	})
}

func Test_job_parseClaims(t *testing.T) {
	n := new(big.Int)
	n, _ = n.SetString("23648271657106453702098841732073788291532737392306604587860650446757306564466381364830857011274527540070651468113486960514163897275777177717466786889375898552139336877707065290374442122684104815548349922622578914529273358894676394378991537856270829263472326566033341246002906926997050938988620778948738899041359146495682618114997520066322627837153551839387802883674510090916769974408762318103732102388373033874929324912398478133782307860465417827258830278539823429425375765133083338284554227468113368339505638355776984713752572292451297353783196607813958264972715696721578480782668352225529538321595051179616947854491", 10)
	public := &rsa.PublicKey{
		N: n,
		E: 65537,
	}

	tests := map[string]jwtTest{
		"successfully parse jwt and claims": {
			job: job{
				encoded:            workingJWT,
				jobID:              "123",
				claimEnvValidation: false,
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertClaims: func(t *testing.T, c Claims) {
				c.OverrideClaims = OverrideClaims{}

				assert.Equal(t, Claims{
					UserLogin:      "user",
					ProjectPath:    "group/project",
					JobID:          "123",
					NamespaceID:    "1001",
					ProjectID:      "2002",
					UserEmail:      "user@example.com",
					PipelineSource: "",
					PipelineID:     "456",
					UserID:         "789",
				}, c)
			},
		},
		"value size validation failed": {
			job: job{
				encoded:            refTypeSize,
				jobID:              "123",
				claimEnvValidation: true,
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "Error:Field validation for 'RefType' failed on the 'max' tag")
				}
			},
		},
		"different job id provided via jwt and runner": {
			job: job{
				encoded: workingJWT,
				jobID:   "456",
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "stolen JWT detected")
			},
		},
		"invalid job claims in jwt": {
			job: job{
				encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6InNsdWcvdGVzdCIsImpvYl9pZCI6MTIzLCJleHAiOjMyNDkxODIwMTYwfQ.GCZ02BpMdbUkXrXCd1zBZWWco7p8QMPXjRdxCb9ICGeGxxtxF4eUeYgjz-5XFLyLUuL3JbxuT5atm8ckMR_DUr6Ami6knDGFvWUjHD007RT9lUghBdqDOp--2eTNRbI4ivwU87oC-sfKx9F9X9CTTN85fLQesx6STm7GQRzHMFJCmMN2p0fdxAZMQcmoSJioe7YYLxPvNyu4ylHYZHA6WJ0FzEwUj23R1Cwm74H59b4O47DSILH8T49lEJRvp_WGYkFPhREnldYcQtWX0BJHL-hrpYfUy6WjQdviPj_X7jnKmsG5madX_dkvWDRRQ_0SLfeqiQlG6pqpAEjjMGlnoA",
				jobID:   "123",
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"json: cannot unmarshal number into Go struct field Claims.job_id of type string",
				)
			},
		},
		"expired jwt provided": {
			job: job{
				encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyIsImV4cCI6OTY2NzI0OTY4fQ.AIhO38b9z253o5sR4BEXQEKKotNSjXtUsL8dp4nBy5gSrKWvjXpC5diKriIon46DR77a2ibqyk0XzLdjU-yL0JJ2EE4TF7HIiCSeQpIzENssaidhWUz2WT-pmNL8kwUmFMBOIa2L-6WhtqXx54V1qatnl8T0vVjlb1rzvGDbDWeyEnI_FKkJ05OQTWUk2JNx8bMSOXnuounTWDUM-mlPcfdV-hdJvVdO621yuvqiBpLZebTCcJazI2YauSV024-OkrBOAlPkbU9CF7IYWgA1QjqUrTXqzd2_CmQb-gbX01NgXNf8KILgY8daFnoqCHzcnASjKyz3_wx3IGulF9D1Fg",
				jobID:   "123",
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "token expired by")
				}
			},
		},
		"empty jwt payload": {
			job: job{
				encoded: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyJ9.RpOSVPNgvoIDceZdnN1Myiavym7rFMdm0glsvfbevR44r2OpbX2voABcP0ySJxN7sPzq7LVzr55Yq-FL9zGs0m8vSgHLaBLbnm2EN20vL58YwtML0DaO1AI4sukWEMiFCkt1XAgRbDIQ9JNKmHgQM-JQKvxsc9bWXuJIroLB5RwvZFCWZaMW2Bta9PEA5AoK-ulVjqnPufDE5Zgy76FHFGzdHMS1-aeJVZWJe2CYBoapbncvFcNLdWtX7YBFTPjmsFkh0zTiYVCtkqbtnARweLwDw90bRfrplIavnhj7HB2wgfyxBt6erszGP6SkgVqnoF4oolbxL2creNU5mfmOpQ",
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "missing expiration (exp)")
			},
		},
		"incorrect jwt subject": {
			job: job{
				encoded:      badSubject,
				jobID:        "123",
				supportJWTV1: true,
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"subject (sub) conflicts with JobID",
				)
			},
		},
		"missing jwt nbf": {
			job: job{
				encoded: missingNBF,
				jobID:   "123",
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"missing not valid before (nbf)",
				)
			},
		},
		"incorrect signing method": {
			job: job{
				encoded: hsJWT,
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unexpected signing method: HS256")
			},
		},
		"missing required": {
			job: job{
				encoded:            workingJWT,
				jobID:              "123",
				claimEnvValidation: true,
			},
			key: public,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.job.parseClaims(tt.key)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertClaims != nil {
				tt.assertClaims(t, got)
			}
		})
	}

}
