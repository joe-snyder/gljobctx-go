// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package gljobctx

import (
	"crypto/rsa"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go/internal/web"
)

/*
JWT test constants created using https://jwt.io and the public/private key pair
made available there at time of creation. This may change in the future so please
find the public/private key below. Note that these values are already observed in
existing JWKS endpoint mocks.

Public:
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1SU1LfVLPHCozMxH2Mo
4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0/IzW7yWR7QkrmBL7jTKEn5u
+qKhbwKfBstIs+bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyeh
kd3qqGElvW/VDL5AaWTg0nLVkjRo9z+40RQzuVaE8AkAFmxZzow3x+VJYKdjykkJ
0iT9wCS0DRTXu269V264Vf/3jvredZiKRkgwlL9xNAwxXFg0x/XFw005UWVRIkdg
cKWTjpBP2dPwVZ4WWC+9aGVd+Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbc
mwIDAQAB
-----END PUBLIC KEY-----

Private:
-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC7VJTUt9Us8cKj
MzEfYyjiWA4R4/M2bS1GB4t7NXp98C3SC6dVMvDuictGeurT8jNbvJZHtCSuYEvu
NMoSfm76oqFvAp8Gy0iz5sxjZmSnXyCdPEovGhLa0VzMaQ8s+CLOyS56YyCFGeJZ
qgtzJ6GR3eqoYSW9b9UMvkBpZODSctWSNGj3P7jRFDO5VoTwCQAWbFnOjDfH5Ulg
p2PKSQnSJP3AJLQNFNe7br1XbrhV//eO+t51mIpGSDCUv3E0DDFcWDTH9cXDTTlR
ZVEiR2BwpZOOkE/Z0/BVnhZYL71oZV34bKfWjQIt6V/isSMahdsAASACp4ZTGtwi
VuNd9tybAgMBAAECggEBAKTmjaS6tkK8BlPXClTQ2vpz/N6uxDeS35mXpqasqskV
laAidgg/sWqpjXDbXr93otIMLlWsM+X0CqMDgSXKejLS2jx4GDjI1ZTXg++0AMJ8
sJ74pWzVDOfmCEQ/7wXs3+cbnXhKriO8Z036q92Qc1+N87SI38nkGa0ABH9CN83H
mQqt4fB7UdHzuIRe/me2PGhIq5ZBzj6h3BpoPGzEP+x3l9YmK8t/1cN0pqI+dQwY
dgfGjackLu/2qH80MCF7IyQaseZUOJyKrCLtSD/Iixv/hzDEUPfOCjFDgTpzf3cw
ta8+oE4wHCo1iI1/4TlPkwmXx4qSXtmw4aQPz7IDQvECgYEA8KNThCO2gsC2I9PQ
DM/8Cw0O983WCDY+oi+7JPiNAJwv5DYBqEZB1QYdj06YD16XlC/HAZMsMku1na2T
N0driwenQQWzoev3g2S7gRDoS/FCJSI3jJ+kjgtaA7Qmzlgk1TxODN+G1H91HW7t
0l7VnL27IWyYo2qRRK3jzxqUiPUCgYEAx0oQs2reBQGMVZnApD1jeq7n4MvNLcPv
t8b/eU9iUv6Y4Mj0Suo/AU8lYZXm8ubbqAlwz2VSVunD2tOplHyMUrtCtObAfVDU
AhCndKaA9gApgfb3xw1IKbuQ1u4IF1FJl3VtumfQn//LiH1B3rXhcdyo3/vIttEk
48RakUKClU8CgYEAzV7W3COOlDDcQd935DdtKBFRAPRPAlspQUnzMi5eSHMD/ISL
DY5IiQHbIH83D4bvXq0X7qQoSBSNP7Dvv3HYuqMhf0DaegrlBuJllFVVq9qPVRnK
xt1Il2HgxOBvbhOT+9in1BzA+YJ99UzC85O0Qz06A+CmtHEy4aZ2kj5hHjECgYEA
mNS4+A8Fkss8Js1RieK2LniBxMgmYml3pfVLKGnzmng7H2+cwPLhPIzIuwytXywh
2bzbsYEfYx3EoEVgMEpPhoarQnYPukrJO4gwE2o5Te6T5mJSZGlQJQj9q4ZB2Dfz
et6INsK0oG8XVGXSpQvQh3RUYekCZQkBBFcpqWpbIEsCgYAnM3DQf3FJoSnXaMhr
VBIovic5l0xFkEHskAjFTevO86Fsz1C2aSeRKSqGFoOQ0tmJzBEs1R6KqnHInicD
TQrKhArgLXX4v3CddjfTRJkFWDbE/CkvKZNOrcf1nhaGCPspRJj2KUkj1Fhl9Cnc
dn/RsYEONbwQSjIfMPkvxF+8HQ==
-----END PRIVATE KEY-----

*/

const (
	// workingJWT header/signature/claims correct.
	workingJWT = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyIsImV4cCI6NDEwMjQ3MzY2MSwibmJmIjoxNjA5NDYyODYxfQ.G4gccOuvfrqZDA78jxLKPa7Wic9OvcWcrkP-w6o9hdlW0keUDMS0IywuecziEaInjyyvXKIhNAY1dn6klDI2rVjMF9MuyBhO5gM7YfB6elDibyy1B10JtiCyX7ZdcLKxuVVuntN-AbQgAUoGIDgwgnkDFNr1HxKSBYJocibe14uDlV8lcOqIdJ8L-88ypzdlS-uyCf21-itfqb27dSY3Fm5RKNt6fkLuw5N4BqghxtyUm9Iz1pOHRQQaQWYPRcP25yfmdVCX59gLdEwbOXv9xnyQL65sBW62ZuE4ylz2lLyypG4SrRwogrZyGbb2Xyv9GDwtCNEEoFDdN3Ccs242YQ"
	// hsJWT signed with HS256
	hsJWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyIsImV4cCI6MzI0OTE4MjAxNjB9.o4K2IPC77pcecydgIgPLb1fCYjMwSejkjeuGoNITx_Q"
	// badSubject header/signature correct but subject (sub) incorrect in claims.
	badSubject = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzQ1NiIsImV4cCI6NDEwMjQ3MzY2MSwibmJmIjoxNjA5NDYyODYxfQ.C6KYLlAzCPviMwWoucVrsaba55nkTk6Z0-bkB7rgYxSw72T2rX1GdKCJE5u-cnoFUPFC3l3hJWNSfCTIG5EFBk30iA3ZF-MeJhRUEEf6xnQxjdDWljzJ6rXM0Kkv1fVyu9LXxgu5vbZ6-LybqGZpfXMoxEIV18Ru6cjCKjfW7Z5guHXUH8zeh2uzWROffENU50xz3v1mxRPbUwRdEGXlTjI4jBLhsFKYsVtSHfRmxFxS7a7I7kzujdouyEhxl8HNJpzjw-umaNtRLxb6BfQw1ASBVIZai-lgSYNS7v4X76-kkydOudiXK53_vvG7s8aXfBxOiz1izvgcOEkVu_BFFA"
	// missingNBF lacks a defined NotBefore field
	missingNBF = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzQ1NiIsImV4cCI6NDEwMjQ3MzY2MX0.akMvfW2fZc_pj2QIVyxcRBoI5rO0-__QkXiPxOurtZSyBQBC3MXe1qN31Oi-XP3rfzxftpyqOT6qGBLoCvoud2jl_eDrbqN05tmCnraI-zljIFaF6jv8QClI1TK9Pq-sN21ImN3-W6seqhaa6LqGSLwkCE3N6810O5t7xjITmIO5O-9paRhqoH6T_cUXHYAOdPxKDpuyo9VRLMBM8f8rRlsLeTy-VAwUnbz9wf18_3feqcaa_nKG6sfnmJ4nRki5e6oHbb2zKRT50TUOnKgNUcJpXT-8gyx5gp1bKHtnWeFlngedBRaUaSuPt4fXalDcYB4BK-nzBq-tIlriGj1ENg"
	// refTypeSize invalid RefType string size
	refTypeSize = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfcGF0aCI6Imdyb3VwIiwibmFtZXNwYWNlX2lkIjoiMTAwMSIsImpvYl9pZCI6IjEyMyIsInBpcGVsaW5lX2lkIjoiNDU2IiwicHJvamVjdF9pZCI6IjIwMDIiLCJ1c2VyX2lkIjoiNzg5IiwidXNlcl9lbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJmZWRlcmF0ZWRfdXNlcm5hbWUiOiJmZWRfdXNlciIsInJlZiI6Im1haW4iLCJyZWZfdHlwZSI6IlNMNjBUSVZMOFEwU0paRlBFNzVLQjJRVUtQQ0hXVUMyS0NOTDlNTThDN09MQkFERzNaSktJMVFMSlVNTldFQ0FPUkQyMjBBM0pYWSIsInJlZl9wcm90ZWN0ZWQiOiJ0cnVlIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyIsImV4cCI6NDEwMjQ3MzY2MSwibmJmIjoxNjA5NDYyODYxfQ.T4m8bcHhHMYdRavw1kk3Ku4YV97pD7eRquDcE8MvJuiFHwZ7ObK_w57ayk4cKxFk7L2zuXzKmcq-ymnEJW-WKFSz4A3ndVfA_OgOzrcJJ_YSv0Il1AW7d-XTnh9OPQuQFLWtnkMYv3ysOuAktGuv5bpVNfG-NmzcwSy2sf783IyPPmw_xzDaWgO3ejxzYkh_AHV2uHkvotAH6xe7RVzEx_F1e2qd5vt0Gbo6IqW8-kfvcqauWr5bAKb8sodUhMHoKtbP2YEZbCx9xcuNeBNDOSwF3J0Abn11L9n5RDlJNwTiqXoRlzyTE3ZiSvIRlexN1M9W7MSRaW5OlKBrFfZ3gg"
)

type jwtTest struct {
	encoded   string
	jobID     string
	serverURL string
	expDelay  time.Duration

	claims Claims
	client web.Client
	job    job
	key    *rsa.PublicKey
	opt    Options

	assertError  func(*testing.T, error)
	assertClaims func(*testing.T, Claims)
}

func TestOptions_ValidateJWT(t *testing.T) {
	tlsFile := t.TempDir() + "/ca.file"
	_ = ioutil.WriteFile(tlsFile, []byte(`-----BEGIN CERTIFICATE-----
example...
-----END CERTIFICATE-----
`), 0700)

	tests := map[string]jwtTest{
		"unable to located tls file": {
			opt: Options{
				TLSCAFile: t.TempDir() + "/missing.cert",
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "invalid TLS CA file supplied")
				}
			},
		},
		"detect invalid delay": {
			opt: Options{
				TLSCAFile: tlsFile,
				ExpDelay:  maxDelay + 1*time.Minute,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "configured ExpDelay surpasses maximum")
				}
			},
		},
		"observe error in GET": {
			opt:       Options{},
			encoded:   workingJWT,
			serverURL: "127.0.0.1",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "unable to retrieve key from JWKS endpoint")
				}
			},
		},
		"observe error in header": {
			opt: Options{
				TLSCAFile: tlsFile,
				ExpDelay:  maxDelay - 1*time.Minute,
			},
			encoded:   hsJWT,
			serverURL: "https://gitlab.example.com",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "unable to validate JWT header")
				}
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.opt.ValidateJWT(tt.encoded, tt.serverURL)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertClaims != nil {
				tt.assertClaims(t, got)
			}
		})
	}
}

func Test_Validator_GitLabJWT(t *testing.T) {
	tlsFile := os.Getenv("CI_SERVER_TLS_CA_FILE")
	encoded := os.Getenv("CI_JOB_JWT")
	encodedV1 := os.Getenv("CI_JOB_JWT_V1")
	encodedV2 := os.Getenv("CI_JOB_JWT_V2")
	server := os.Getenv("CI_SERVER_URL")
	jobID := os.Getenv("CI_JOB_ID")

	if tlsFile == "" && encoded == "" && server == "" {
		t.Skip("required CI env not found")
	}

	t.Run("test full workflow during GitLab CI", func(t *testing.T) {
		claims, err := Options{
			JobID:     jobID,
			TLSCAFile: tlsFile,
		}.ValidateJWT(encoded, server)

		assert.NoError(t, err)
		if err == nil {
			assert.Equal(t, jobID, claims.JobID)
		}
	})

	if encodedV1 != "" {
		t.Run("test v1 workflow during GitLab CI env validate", func(t *testing.T) {
			claims, err := Options{
				JobID:               jobID,
				TLSCAFile:           tlsFile,
				ClaimsEnvValidation: true,
			}.ValidateJWT(encodedV1, server)

			assert.NoError(t, err)
			if err == nil {
				assert.Equal(t, jobID, claims.JobID)
			}
		})
	}

	if encodedV2 != "" {
		t.Run("test v2 workflow during GitLab CI env validate", func(t *testing.T) {
			claims, err := Options{
				JobID:               jobID,
				TLSCAFile:           tlsFile,
				ClaimsEnvValidation: true,
			}.ValidateJWT(encodedV2, server)

			assert.NoError(t, err)
			if err == nil {
				assert.Equal(t, jobID, claims.JobID)
			}
		})
	}
}
